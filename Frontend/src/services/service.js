import axios from 'axios'
// create an axios instance
export const service = axios.create({

});

const baseUrl = 'https://c-portfolio-int.misfit.com/v2/rpc/auth';
service.interceptors.response.use(
  response => response,
  error => {
    switch (error.response.status) {
      case 401:
        localStorage.removeItem('authUser');
        service.redirectTo(document, '/login')
        break
      case 404:
        service.redirectTo(document, '/404')
        break
      default:
        service.redirectTo(document, '/500')
        break
    }
    return Promise.reject(error)
  })
service.redirectTo = (document, path) => {
  document.location = path
}
service.api = {}

service.api.get = (path, callback) => {
  let token = localStorage.getItem('authUser');
  let config = {
    headers: {
      Authorization: 'Bearer ' + token
    }
  };
  return service.get(baseUrl + path, config);
}

service.api.post = (path, params, config, callback) => {
  let token = localStorage.getItem('authUser');
  if (!config) {
    config = {
    };
  }

  if (!config.headers) {
    config.headers = {};
  }
  config.headers.Authorization = 'Bearer ' + token;
  return service.post(baseUrl + path, params, config)
}
